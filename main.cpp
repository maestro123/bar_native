#include <iostream>
#include <stdio.h>
#include <fstream>
#include "model/book_info.h"
#include "fb2/fb2_parser.h"

using namespace std;

#define BUFFER_SIZE 100000

int parse_xml(char *buff, size_t buff_size) {

    BookParser parser;
    FB2Parser parserHolder;

    parser.parse(parserHolder, buff);

    cout << "Title: " << parserHolder.Info.Title << endl;
    ofstream mfile;

    mfile.open("X:/test_out.txt");
    mfile << parserHolder.Info.Title << endl;
    mfile.close();

    parserHolder.Info.TextModel->Dump();
    return 0;
}

int main() {

    cout << "Hello" << endl;

    int result;
    char buffer[BUFFER_SIZE];
    FILE *fp;
    fp = fopen("X:/Books/Cathingfire.fb2", "r");
    fread(buffer, sizeof(char), BUFFER_SIZE, fp);
    result = parse_xml(buffer, BUFFER_SIZE);
    printf("Result is %i\n", result);
    return 0;
}
