//
// Created by artyom on 8.4.15.
//

#ifndef BFREADER_BOOK_PARSER_CPP
#define BFREADER_BOOK_PARSER_CPP

#include <stdio.h>
#include <string.h>
#include <iostream>
#include "book_parser.h"
#include "../expat/expat.h"

using namespace std;

static int depth = 0;

void BookParser::start_element(void *data, const char *element, const char **attribute) {
    depth++;
    ((BookParserHolder *) data)->onElementStart(element, attribute);
}

void BookParser::end_element(void *data, const char *element) {
    ((BookParserHolder *) data)->onElementEnd(element);
    depth--;
}

void BookParser::handle_data(void *data, const char *content, int length) {
    char *tmp = (char *) malloc(length);
    strncpy(tmp, content, length);
    tmp[length] = '\0';
    ((BookParserHolder *) data)->onHandleData(tmp, length);
}

void BookParser::unknown_encoding(void *data, const XML_Char *name, XML_Encoding *encoding) {
    cout << "unknown encoding: " << name << endl;
}


int BookParser::parse(BookParserHolder &holder, char *path) {
    XML_Parser parser = XML_ParserCreate(NULL);
    XML_UseForeignDTD(parser, XML_TRUE);
    XML_SetUserData(parser, &holder);
    XML_SetElementHandler(parser, start_element, end_element);
    XML_SetCharacterDataHandler(parser, handle_data);

    if (XML_Parse(parser, path, strlen(path), XML_FALSE) == XML_STATUS_ERROR) {
        printf("Error: %s\n", XML_ErrorString(XML_GetErrorCode(parser)));
    }

    XML_ParserFree(parser);
    return 0;
}

void BookParserHolder::onElementStart(const char *element, const char **attribute) {
}

void BookParserHolder::onElementEnd(const char *element) {
}

void BookParserHolder::onHandleData(const char *content, int length) {
}


#endif //BFREADER_BOOK_PARSER_CPP