//
// Created by artyom on 8.4.15.
//

#include "text_model.h"
#include "string.h"
#include "../expat/expat_external.h"
#include "../expat/expat.h"

#ifndef BFREADER_BOOK_PARSER_H
#define BFREADER_BOOK_PARSER_H

class BookParserHolder {

public:

    virtual void onElementStart(const char *element, const char **attribute);

    virtual void onElementEnd(const char *element);

    virtual void onHandleData(const char *content, int length);

    bool match(char const *c, char const *t) {
        return strcmp(c, t) == 0;
    }

};

class BookParser {

private:

    static void start_element(void *data, const char *element, const char **attribute);

    static void end_element(void *data, const char *element);

    static void handle_data(void *data, const char *content, int length);

    static void unknown_encoding(void *, const XML_Char *name, XML_Encoding *encoding);

public:

    int parse(BookParserHolder &holder, char *path);

    friend class BookParserHolder;

    static char *TagData;

};


#endif //BFREADER_BOOK_PARSER_H

