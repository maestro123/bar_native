//
// Created by artyom on 8.4.15.
//

#include <vector>
#include "text_var.h"
#include "shared_ptr.h"

#ifndef BFREADER_TEXT_MODE_H
#define BFREADER_TEXT_MODE_H

using namespace std;

class BPartObject {

public:
    std::vector<shared_ptr<TextObject>> Sections;

    TextObject LastObject() {
        return *Sections[Sections.size() - 1];
    }

};

class BTextModel {

    shared_ptr<BPartObject> LastPart;
    shared_ptr<ContentObject> LastContent;

    int ParagraphNumber = -1;

public:

    shared_ptr<Paragraph> LastParagraph;
    std::vector<shared_ptr<BPartObject>> Parts;

    void addPart() {
        BPartObject partObject;
        LastPart = shared_ptr<BPartObject>(&partObject);
        Parts.push_back(shared_ptr<BPartObject>(&partObject));
    }

    void addContent(ContentObject contentObject) {
        LastContent = shared_ptr<ContentObject>(&contentObject);
        LastPart->Sections.push_back(shared_ptr<TextObject>(&contentObject));
    }

    void addParagraph(Paragraph paragraph) {
        ParagraphNumber++;
        paragraph.Number = ParagraphNumber;
        LastParagraph = shared_ptr<Paragraph>(&paragraph);
        LastContent->TextObjects.push_back(shared_ptr<TextObject>(&paragraph));
    }

    void addTextObject(TextObject textObject) {
        LastContent->TextObjects.push_back(shared_ptr<TextObject>(&textObject));
    }

    void Dump() {
        std::cout << "Parts: " << Parts.size() << endl << "Last part size: " << LastPart->Sections.size() << endl;
        for (int i = 0; i < Parts.size(); i++) {
            vector<shared_ptr<TextObject>> objects = ((BPartObject &) Parts[i]).Sections;
            std::cout << "Part #" << i << " size: " << objects.size() << std::endl;
//            for (int j = 0; j < objects.size(); ++j) {
//
//            }
        }
    }


};

#endif //BFREADER_TEXT_MODE_H