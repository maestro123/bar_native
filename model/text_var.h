//
// Created by artyom on 8.4.15.
//

#ifndef BFREADER_TEXT_VAR_H
#define BFREADER_TEXT_VAR_H

#include <iostream>
#include "shared_ptr.h"

using namespace std;

class TextObject {

};

class Paragraph : public TextObject {

public:

    char *Text;
    int Number;

    char *Language;
};

class EmptyLine : public TextObject {

};

class ContentObject : public TextObject {

public:
    std::vector<shared_ptr<TextObject>> TextObjects;

};

class Title : public ContentObject{

};

class Section : public ContentObject {

public:

    int Id;

};

class Epigraph : public TextObject {

public:

    std::vector<TextObject> TextObjects;

};

class Style : public TextObject {

public:

    char *Name;
    char *Lang;

    int charStart;
    int charEnd;

};

class Link : public TextObject {

public:

    char *Name;
    char *Type;
    int charStart;
    int charEnd;

};

class Poem : public TextObject {

public:
    std::vector<TextObject> TextObjects;

};

class Stanza : public TextObject {

public:
    std::vector<TextObject> TextObjects;

};

class Verse : public TextObject {

public:
    char *Text;

};

#endif //BFREADER_TEXT_VAR_H