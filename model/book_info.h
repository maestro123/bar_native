//
// Created by artyom on 8.4.15.
//

#ifndef BFREADER_BOOK_INFO_H
#define BFREADER_BOOK_INFO_H


#include "vector"
#include "text_model.h"
#include "shared_ptr.h"

class BAuthor {

public:
    std::string FirstName;
    std::string MiddleName;
    std::string LastName;
    std::string NickName;
    std::string HomePage;
    std::string Email;
    std::string Id;

};

class BSequence {

public:
    std::string Name;
    int Number;
};

class BDocumentInfo {

public:

    BAuthor Author;
    std::string UsedProgramm;
    std::string Date;
    std::string SourceUrl;
    std::string SourceOcr;
    std::string Id;
    std::string Version;
    std::string History;

};

class BPublishInfo {

public:

    std::string Name;
    std::string Publisher;
    std::string City;
    std::string Year;
    std::string ISBN;

};

class BookInfo {

public:

    std::string Title;
    std::string Date;
    std::string ImagePath;
    std::string Language;
    std::string SourceLanguage;

    shared_ptr<BAuthor> Author = shared_ptr<BAuthor>(new BAuthor);
    shared_ptr<BAuthor> Translator;
    shared_ptr<BDocumentInfo> DocumentInfo = shared_ptr<BDocumentInfo>(new BDocumentInfo);
    shared_ptr<BPublishInfo> PublishInfo = shared_ptr<BPublishInfo>(new BPublishInfo);

    std::vector<BSequence> Sequence;
    std::vector<std::string> Genres;
    std::vector<std::string> KeyWords;

    shared_ptr<BTextModel> TextModel;

    friend class BAuthor;

    friend class BSequence;

    friend class BDocumentInfo;

    friend class BPublishInfo;

};


#endif //BFREADER_BOOK_INFO_H