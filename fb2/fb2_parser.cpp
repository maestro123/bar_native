//
// Created by artyom on 8.4.15.
//

#include <iostream>
#include <string.h>
#include "fb2_parser.h"

#ifndef BFREADER_FB2_PARSER_H
#define BFREADER_FB2_PARSER_H

static char *TagData;

void FB2Parser::onElementStart(const char *element, const char **attribute) {
    cout << "onElementStart: " << element << endl;
    if (match(element, TAG_DESCRIPTION)) {
        onDescription = true;
    } else if (match(element, TAG_TITLE_INFO)) {
        onTitleInfo = true;
    } else if (match(element, TAG_AUTHOR)) {
        onAuthor = true;
    } else if (match(element, TAG_DOCUMENT_INFO)) {
        onDocumentInfo = true;
    } else if (match(element, TAG_PUBLISH_INFO)) {
        onPublishInfo = true;
    } else if (match(element, TAG_BODY)) {
        onBody = true;
        Info.TextModel->addPart();
    } else if (match(element, TAG_SEQUENCE)) {
        if (onTitleInfo) {
            for (int i = 0; attribute[i]; i += 2) {
                if (match(attribute[i], TAG_NAME)) {
                    Info.KeyWords.push_back((char *) attribute[i + 1]);
                }
            }
        }
    } else if (match(element, TAG_TITLE)) {
        if (onBody) {
            onBodyTitle = true;
            Title objTitle;
            Info.TextModel->addContent(objTitle);
        }
    } else if (match(element, TAG_SECTION)) {
        if (onBody) {
            onBodySection = true;
            Section section;
            Info.TextModel->addContent(section);
        }
    } else if (match(element, TAG_P)) {
        if (onBody) {
            Paragraph paragraph;
            Info.TextModel->addParagraph(paragraph);
        }
    } else if (match(element, TAG_EMPTY_LINE)) {
        if (onBody) {
            EmptyLine emptyLine;
            Info.TextModel->addTextObject(emptyLine);
        }
    } else if (match(element, TAG_EMPHASIS)) {

    }
}

void FB2Parser::onElementEnd(const char *element) {
    cout << "onElementEnd: " << element << endl;
    if (match(element, TAG_DESCRIPTION)) {
        onDescription = false;
    } else if (match(element, TAG_TITLE_INFO)) {
        onTitleInfo = false;
    } else if (match(element, TAG_DOCUMENT_INFO)) {
        onDocumentInfo = false;
    } else if (match(element, TAG_PUBLISH_INFO)) {
        onPublishInfo = false;
    } else if (match(element, TAG_AUTHOR)) {
        onAuthor = false;
    } else if (match(element, TAG_BODY)) {
        onBody = false;
    } else if (!strcmp(element, TAG_BOOK_TITLE)) {
        Info.Title = TagData;
    } else if (match(element, TAG_GENRE)) {
        Info.Genres.push_back(TagData);
    } else if (match(element, TAG_FIRST_NAME)) {
        if (onAuthor) {
            if (onTitleInfo) {
                Info.Author->FirstName = TagData;
            } else if (onDocumentInfo) {
                Info.DocumentInfo->Author.FirstName = TagData;
            }
        }
    } else if (match(element, TAG_LAST_NAME)) {
        if (onAuthor) {
            if (onTitleInfo) {
                Info.Author->LastName = TagData;
            } else if (onDocumentInfo) {
                Info.DocumentInfo->Author.LastName = TagData;
            }
        }
    } else if (match(element, TAG_MIDDLE_NAME)) {
        if (onAuthor) {
            if (onTitleInfo) {
                Info.Author->MiddleName = TagData;
            } else if (onDocumentInfo) {
                Info.DocumentInfo->Author.MiddleName = TagData;
            }
        }
    } else if (match(element, TAG_EMAIL)) {
        if (onAuthor) {
            if (onTitleInfo) {
                Info.Author->Email = TagData;
            } else if (onDocumentInfo) {
                Info.DocumentInfo->Author.Email = TagData;
            }
        }
    } else if (match(element, TAG_ID)) {
        if (onAuthor) {
            if (onTitleInfo) {
                Info.Author->Id = TagData;
            } else if (onDocumentInfo) {
                Info.DocumentInfo->Author.LastName = TagData;
            }
        }
    } else if (match(element, TAG_DATE)) {
        if (onTitleInfo) {
            Info.Date = TagData;
        }
    } else if (match(element, TAG_LANG)) {
        if (onTitleInfo) {
            Info.Language = TagData;
        }
    } else if (match(element, TAG_BOOK_NAME)) {
        if (onPublishInfo) {
            Info.PublishInfo->Name = TagData;
        }
    } else if (match(element, TAG_PUBLISHER)) {
        if (onPublishInfo) {
            Info.PublishInfo->Publisher = TagData;
        }
    } else if (match(element, TAG_CITY)) {
        if (onPublishInfo) {
            Info.PublishInfo->City = TagData;
        }
    } else if (match(element, TAG_YEAR)) {
        if (onPublishInfo) {
            Info.PublishInfo->Year = TagData;
        }
    } else if (match(element, TAG_ISBN)) {
        if (onPublishInfo) {
            Info.PublishInfo->ISBN = TagData;
        }
    } else if (match(element, TAG_TITLE)) {
        if (onBodyTitle) {
            onBodyTitle = false;
        }
    } else if (match(element, TAG_SECTION)) {
        if (onBodySection) {
            onBodySection = false;
        }
    } else if (match(element, TAG_P)) {
        if (onBody) {
            Info.TextModel->LastParagraph->Text = TagData;
        }
    } else if (match(element, TAG_EMPHASIS)) {

    }
}

void FB2Parser::onHandleData(const char *content, int length) {
    TagData = (char *) content;
}


#endif //BFREADER_FB2_PARSER_H
