//
// Created by artyom on 8.4.15.
//

#include "../model/book_parser.h"
#include "../model/text_model.h"
#include "../model/book_info.h"

#ifndef BFREADER_HEADER_FILE_H
#define BFREADER_HEADER_FILE_H


class FB2Parser : public BookParserHolder {

    const char *TAG_DESCRIPTION = "description";
    const char *TAG_TITLE_INFO = "title-info";
    const char *TAG_DOCUMENT_INFO = "document-info";
    const char *TAG_PUBLISH_INFO = "publish-info";
    const char *TAG_GENRE = "genre";
    const char *TAG_AUTHOR = "author";
    const char *TAG_FIRST_NAME = "first-name";
    const char *TAG_MIDDLE_NAME = "middle-name";
    const char *TAG_LAST_NAME = "last-name";
    const char *TAG_EMAIL = "email";
    const char *TAG_ID = "id";
    const char *TAG_BOOK_TITLE = "book-title";
    const char *TAG_BOOK_NAME = "book-name";
    const char *TAG_DATE = "date";
    const char *TAG_COVER_PAGE = "coverpage";
    const char *TAG_LANG = "lang";
    const char *TAG_SEQUENCE = "sequence";
    const char *TAG_NAME = "name";
    const char *TAG_PUBLISHER = "publisher";
    const char *TAG_CITY = "city";
    const char *TAG_YEAR = "year";
    const char *TAG_ISBN = "isbn";
    const char *TAG_BODY = "body";

    const char *TAG_IMAGE = "image"; //usually at attributes

    const char *TAG_TITLE = "title";
    const char *TAG_SECTION = "section";
    const char *TAG_P = "p";
    const char *TAG_EMPTY_LINE = "empty-line";
    const char *TAG_EMPHASIS = "emphasis";

public:
    virtual void onElementStart(const char *element, const char **attribute);

    virtual void onElementEnd(const char *element);

    virtual void onHandleData(const char *content, int length);

public:

    bool onDescription = false;
    bool onTitleInfo = false;
    bool onAuthor = false;
    bool onTranslator = false;
    bool onSequence = false;
    bool onDocumentInfo = false;
    bool onPublishInfo = false;
    bool onBody = false;
    bool onBodyTitle = false;
    bool onBodySection = false;
    bool onParagraph = false;

    BookInfo Info;

};

#endif //BFREADER_HEADER_FILE_H